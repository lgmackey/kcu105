`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/26/2024 09:24:14 PM
// Design Name: 
// Module Name: uart_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module uart_tb(

    );
logic       clk_in1_p;
logic       clk_in1_n;
logic       clk_out = 0;
logic       rst = 0;
logic [31:0] tx_data;
logic       tx_ready;
logic       tx_valid;
logic       tx_last;
logic       tx_out;
logic [4:0] run_count = 0;

// converts FPGA 125 MHz differential system clock to chosen output clock frequency (100 MHz in this case)
sys_clk_conv inst (
            .clk_out(clk_out), // clock out ports  
            .clk_in1_p(clk_in1_p), // clock in ports
            .clk_in1_n(clk_in1_n)
            );
            
// UUT 
uart_tx uut (
            .clk(clk_out),
            .rst(rst),
            .tx_data(tx_data),
            .tx_data_valid(tx_valid),
            .tx_data_ready(tx_ready),
            .tx_data_last(tx_last),
            .UART_TX(tx_out)
            );
            
// generate input data
lfsr_32bit dut (
            .clk(clk_out),
            .rst(rst),
            .clk_en(tx_ready),
            .data(tx_data)
            );

            
// Clock definition
localparam SYS_CLK_PERIOD = 8000; // clk period = 8us --> 125 MHz (system clock frequency)
localparam CLK_PERIOD = 10000; // clk period = 10us --> 100 Mhz
localparam RST_COUNT = 10; //Clock cycles that reset is high

always begin
    #(SYS_CLK_PERIOD/2) clk_in1_p = ~clk_in1_p;
    clk_in1_n = ~clk_in1_p;
end
   
// reset definition
initial begin
   clk_in1_p <= 0;
   rst = 1;
   #(RST_COUNT*CLK_PERIOD);
   @(posedge clk_out);
   rst = 0;
end

localparam PACKET_COUNTER_MAX = 10;
localparam PACKET_COUNTER_SIZE = $clog2(PACKET_COUNTER_MAX);

logic [PACKET_COUNTER_SIZE-1:0] packet_counter;

always @ (posedge clk_out) begin
    if (rst) begin
        packet_counter <= '0;
    end
    else begin
        // don't store data unless ready
        if (tx_last & tx_valid % tx_ready) begin
            packet_counter <= '0;
        end
        else if (tx_valid & tx_ready) begin
            packet_counter <= packet_counter + 'd1;
        end
    end
end

assign tx_last = (packet_counter == PACKET_COUNTER_MAX-1) ? 1'b1 : 1'b0;         

assign tx_valid = ~rst;

always @(posedge clk_out) begin
    if (tx_last) begin
        run_count <= run_count + 'd1;
    end
    
    if (run_count >= 'd20) begin
        rst <= 1;
    end
end


endmodule
