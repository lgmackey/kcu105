`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/26/2024 10:03:35 PM
// Design Name: 
// Module Name: lfsr_8bit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module lfsr_32bit
  (
   input 	clk,
   input 	rst,
   input    clk_en,
   output [31:0] data
   );
   
   
   logic [32:0] 	lfsr;
   logic [7:0] 	lfsr_feedback;
   assign lfsr_feedback= {lfsr[31],lfsr[9:3]};
   
   always @(posedge clk) begin
	if (rst) begin
           lfsr <= 32'hFFFFFFFF; 
	end
	else if (clk_en) begin
           lfsr[31:1] <= lfsr[30:0]; 
           lfsr[0] <= ^lfsr_feedback;           
	end
    end
   
   assign data = lfsr;
   
endmodule
