This project should work as a UART transmitter, and is based on the project https://github.com/HDLForBeginners/Examples/tree/main/UART with minor changes.

The changes I made are:
Added the sys_clk_conv IP which converts one of the differential clocks on the FPGA to a single output for use in the rest of the project.
Counted the tx_last signal (goes high on the last 32-bit packet in each set) and stopped transmitting data when it reaches 10.
Tied the tx_last signal to an LED on the eval board. It comes on, but never goes off, so something is not working or is tied to the wrong pins.
